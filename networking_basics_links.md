# 1. Mac addresses
### https://www.pcmag.com/encyclopedia/term/mac-address
- Pretty simple explanation of mac addresses and what they are.
### https://standards.ieee.org/content/dam/ieee-standards/standards/web/documents/tutorials/macgrp.pdf 
- Complicated but deep explanation of the conventions for how mac addresses are assigned and what they are in general.

# 2. Switches
### https://www.cisco.com/c/en/us/solutions/small-business/resource-center/networking/network-switch-vs-router.html
- Easy layman description but biased by being written by a company.
### https://www.cloudflare.com/learning/network-layer/what-is-a-network-switch/
- More in depth but relatively easy to understand, still colored by a company’s biases.

# 3. Ip addresses
### https://whatismyipaddress.com/ip-basics
- Super simple explanation in laymens terms.
### https://computer.howstuffworks.com/internet/basics/what-is-an-ip-address.htm
- This is very detailed and explain the different classes in subnets and how an ip is used locally.

# 4.Routers:
### https://www.lifewire.com/what-is-a-router-2618162
- Easy to understand for laymen and provides links for more detailed explanations.
### https://www.ciscopress.com/articles/article.asp?p=2180210&seqNum=4
- Information on how routers do their routing using DHCP etc. with links to more information on how they work.




